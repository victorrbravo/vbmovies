package com.vbravo.vbmovies;

public class Utils {

    public static String convertToTitle(String title, Double value) {
        String result;

        result = title +  ": " +String.valueOf(value);

        return result;
    }


    public static String convertToTitle(String title, Integer value) {

        String result;

        result = title +  ": " +String.valueOf(value);

        return result;
    }
}

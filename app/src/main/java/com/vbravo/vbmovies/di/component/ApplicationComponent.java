package com.vbravo.vbmovies.di.component;

import android.content.Context;


import com.vbravo.vbmovies.MyApplication;
import com.vbravo.vbmovies.di.module.ContextModule;
import com.vbravo.vbmovies.di.module.RetrofitModule;
import com.vbravo.vbmovies.di.qualifier.ApplicationContext;
import com.vbravo.vbmovies.di.scopes.ApplicationScope;
import com.vbravo.vbmovies.retrofit.APIInterface;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    APIInterface getApiInterface();

    @ApplicationContext
    Context getContext();

    void injectApplication(MyApplication myApplication);
}

package com.vbravo.vbmovies.di.component;

import android.content.Context;

import com.vbravo.vbmovies.MainActivity;
import com.vbravo.vbmovies.di.module.AdapterModule;
import com.vbravo.vbmovies.di.module.MainActivityMvpModule;
import com.vbravo.vbmovies.di.qualifier.ActivityContext;
import com.vbravo.vbmovies.di.scopes.ActivityScope;
import dagger.Component;


@ActivityScope
@Component(modules = {AdapterModule.class, MainActivityMvpModule.class}, dependencies = ApplicationComponent.class)
public interface MainActivityComponent {

    @ActivityContext
    Context getContext();
    void injectMainActivity(MainActivity mainActivity);
}

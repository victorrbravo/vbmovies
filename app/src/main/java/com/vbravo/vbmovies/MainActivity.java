package com.vbravo.vbmovies;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.vbravo.vbmovies.di.component.ApplicationComponent;
import com.vbravo.vbmovies.di.component.DaggerMainActivityComponent;
import com.vbravo.vbmovies.di.component.MainActivityComponent;
import com.vbravo.vbmovies.di.module.MainActivityContextModule;
import com.vbravo.vbmovies.di.module.MainActivityMvpModule;
import com.vbravo.vbmovies.di.qualifier.ActivityContext;
import com.vbravo.vbmovies.di.qualifier.ApplicationContext;
import com.vbravo.vbmovies.mvp.MainActivityContract;
import com.vbravo.vbmovies.mvp.PresenterImpl;
import com.vbravo.vbmovies.pojo.Movie;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View, RecyclerViewAdapter.ClickListener {

    private RecyclerView recyclerView;
    private Spinner typeSpinner;
    private ProgressBar progressBar;
    private LinearLayout principalLayout;
    MainActivityComponent mainActivityComponent;

    @Inject
    public RecyclerViewAdapter recyclerViewAdapter;


    @Inject
    @ApplicationContext
    public Context mContext;

    @Inject
    @ActivityContext
    public Context activityContext;

    @Inject
    PresenterImpl presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ApplicationComponent applicationComponent = MyApplication.get(this).getApplicationComponent();
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .mainActivityContextModule(new MainActivityContextModule(this))
                .mainActivityMvpModule(new MainActivityMvpModule(this))
                .applicationComponent(applicationComponent)
                .build();

        mainActivityComponent.injectMainActivity(this);


        // Selector of Movie Type

        typeSpinner = findViewById(R.id.typeSpinner);

        String[] categorySpinner = new String[] { "Popular", "Rated",
                "Upcoming",};

        ArrayList<String> spinnerArray1 = new ArrayList<>(Arrays.asList(categorySpinner));
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>
                (this, R.layout.support_simple_spinner_dropdown_item,
                        spinnerArray1)
        {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                return super.getDropDownView(position, convertView, parent);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                return super.getView(position, convertView, parent);
            }
        };

        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        typeSpinner.setAdapter(spinnerArrayAdapter1);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String selected  = typeSpinner.getSelectedItem().toString();

                Log.d("Main","checkei...filtering selected: " + selected);
                if (selected.equals("Popular")) {
                    Log.d("Main","check...LoadData..Popular.1");

                    presenter.loadData(0);
                } else if ( selected.equals("Rated")) {
                    Log.d("Main","check...LoadData..Rated...1");

                    presenter.loadData(1);
                } else if (selected.equals("Upcoming")) {
                    Log.d("Main","check...LoadData...1");
                    Log.d("Main","check...LoadData..Upcoming...1");

                    presenter.loadData(2);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        // Selector of Movie Type


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activityContext));
        recyclerView.setAdapter(recyclerViewAdapter);

        progressBar = findViewById(R.id.progressBar);
        principalLayout = findViewById(R.id.principalLayout);



        presenter.loadData(0);


    }

    @Override
    public void launchIntent(String name) {
        Toast.makeText(mContext, name, Toast.LENGTH_LONG).show();
        // startActivity(new Intent(activityContext, DetailActivity.class).putExtra("name", name));
    }

    @Override
    public void showData(Movie data) {

        recyclerView.setAdapter(recyclerViewAdapter);
        Log.d("Consult","consult title:"+ data.getResults().get(0).getTitle());
        recyclerViewAdapter.setData(data.getResults());
    }

    @Override
    public void showError(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        principalLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
        principalLayout.setVisibility(View.VISIBLE);
    }
}

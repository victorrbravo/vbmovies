package com.vbravo.vbmovies.retrofit;

import com.vbravo.vbmovies.pojo.Movie;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface APIInterface {


    @GET("/3/discover/movie?sort_by=popularity.desc&api_key=f64cd532a99334797eab7074d6e1fdf7")
    Observable<Movie> getData();

    @GET("/3/discover/movie?sort_by=vote_count.desc&api_key=f64cd532a99334797eab7074d6e1fdf7")
    Observable<Movie> getRated();


    @GET("/3/movie/upcoming?api_key=f64cd532a99334797eab7074d6e1fdf7")
    Observable<Movie> getUpcoming();

}

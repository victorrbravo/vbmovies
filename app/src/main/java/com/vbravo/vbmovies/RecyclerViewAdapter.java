package com.vbravo.vbmovies;

import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vbravo.vbmovies.pojo.Movie;
import com.vbravo.vbmovies.pojo.Result;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Result> data;
    private RecyclerViewAdapter.ClickListener clickListener;

    @Inject
    public RecyclerViewAdapter(ClickListener clickListener) {
        this.clickListener = clickListener;
        data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTitle.setText(data.get(position).getTitle());
        holder.txtPopularity.setText(Utils.convertToTitle("Pop", data.get(position).getPopularity()));
        holder.txtOriginalLang.setText(data.get(position).getOriginalLanguage());
        holder.txtVoteAverage.setText(Utils.convertToTitle("Vote Avg", data.get(position).getVoteAverage()));
        holder.txtVoteCount.setText(Utils.convertToTitle("Vote Count:", data.get(position).getVoteCount()));
        holder.txtReleaseDate.setText(data.get(position).getReleaseDate());
        holder.txtOverview.setText(data.get(position).getOverview().substring(0,10));
        String posteruri =  "https://image.tmdb.org/t/p/original" + data.get(position).getPosterPath();
        Log.d("Poster", "check...1...poster uri:" + posteruri);
        Picasso.get().load(posteruri).into(holder.posterPath);

    }

    @Override
    public int getItemCount() {
        if (data.size() > 0 ) {
            Log.d("Result", "check_  count:" + data.size());
            return data.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView posterPath;
        private TextView txtTitle;
        private TextView txtPopularity;
        private TextView txtOriginalLang;
        private TextView txtVoteAverage;
        private TextView txtVoteCount;
        private TextView txtOverview;
        private TextView txtReleaseDate;

        private LinearLayout principalContainer;

        ViewHolder(View itemView) {
            super(itemView);

            posterPath = itemView.findViewById(R.id.posterPath);

            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtPopularity = itemView.findViewById(R.id.txtPopularity);
            txtOriginalLang = itemView.findViewById(R.id.txtOriginalLang);
            txtVoteAverage = itemView.findViewById(R.id.txtVoteAverage);
            txtVoteCount = itemView.findViewById(R.id.txtVoteCount);
            txtReleaseDate = itemView.findViewById(R.id.txtReleaseDate);
            txtOverview = itemView.findViewById(R.id.txtOverview);

            principalContainer = itemView.findViewById(R.id.principalContainer);

            principalContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.launchIntent(data.get(getAdapterPosition()).getOverview());
                }
            });
        }
    }

    public interface ClickListener {
        void launchIntent(String name);
    }

    public void setData(List<Result> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }
}


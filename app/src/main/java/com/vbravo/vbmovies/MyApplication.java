package com.vbravo.vbmovies;

import android.app.Activity;
import android.app.Application;

import com.vbravo.vbmovies.di.component.ApplicationComponent;
import com.vbravo.vbmovies.di.component.DaggerApplicationComponent;
import com.vbravo.vbmovies.di.module.ContextModule;


public class MyApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .contextModule(new ContextModule(this)).build();
        applicationComponent.injectApplication(this);

    }


    public static MyApplication get(Activity activity){
        return (MyApplication) activity.getApplication();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}


package com.vbravo.vbmovies.mvp;

import android.util.Log;

import com.vbravo.vbmovies.pojo.Movie;
import com.vbravo.vbmovies.retrofit.APIInterface;

import javax.inject.Inject;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PresenterImpl implements MainActivityContract.Presenter {

    APIInterface apiInterface;
    Observer<Movie> currentObserver;


    MainActivityContract.View mView;

    @Inject
    public PresenterImpl(APIInterface apiInterface, MainActivityContract.View mView) {
        this.apiInterface = apiInterface;
        this.mView = mView;
    }

    @Override
    public void loadData(int consult) {

        mView.showProgress();
        if ( consult == 0 ) {
            currentObserver = new Observer<Movie>() {
                @Override
                public void onCompleted() {
                    mView.showComplete();
                    mView.hideProgress();
                }

                @Override
                public void onError(Throwable e) {
                    mView.showError("Error occurred");
                    mView.hideProgress();
                }

                @Override
                public void onNext(Movie data) {
                    mView.showData(data);
                }
            };

            apiInterface.getData().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(currentObserver);
        } else if ( consult == 1) {

            currentObserver = new Observer<Movie>() {
                @Override
                public void onCompleted() {
                    mView.showComplete();
                    mView.hideProgress();
                }

                @Override
                public void onError(Throwable e) {
                    mView.showError("Error occurred");
                    mView.hideProgress();
                }

                @Override
                public void onNext(Movie data) {
                    mView.showData(data);
                }
            };


            apiInterface.getRated().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(currentObserver);

        } else if (consult == 2) {

            apiInterface.getUpcoming().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Movie>() {
                        @Override
                        public void onCompleted() {
                            mView.showComplete();
                            mView.hideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.showError("Error occurred");
                            mView.hideProgress();
                        }

                        @Override
                        public void onNext(Movie data) {
                            mView.showData(data);
                        }
                    });

        }
    }
}

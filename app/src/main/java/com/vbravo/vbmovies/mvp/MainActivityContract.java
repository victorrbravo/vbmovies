package com.vbravo.vbmovies.mvp;


import com.vbravo.vbmovies.pojo.Movie;

public interface MainActivityContract {
    interface View {
        void showData(Movie data);

        void showError(String message);

        void showComplete();

        void showProgress();

        void hideProgress();
    }

    interface Presenter {
        void loadData(int consult);
    }
}

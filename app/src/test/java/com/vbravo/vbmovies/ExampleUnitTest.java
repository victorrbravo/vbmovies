package com.vbravo.vbmovies;

import org.junit.Test;

import com.vbravo.vbmovies.Utils;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void convertTitle_isCorrect() {
        assertEquals("Avg: 2.0", Utils.convertToTitle("Avg",2.0));
        assertEquals("Vote count: 2.0", Utils.convertToTitle("Vote count",2.0));
        assertEquals("Popularity: 2.0", Utils.convertToTitle("Popularity",2.0));
    }
}